#pragma once

#include <iostream>
#include <string>

#include <opencv2\opencv.hpp>
#include "opencv2/objdetect.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/imgproc.hpp"

using namespace std;
using namespace cv;

class Eye {
public:
	#define RIGHT_EYE_SIDE true
	#define LEFT_EYE_SIDE false

	Eye(Rect, Rect, Point, Point, Point, int, bool);
	Eye();
	Eye(const Eye &);
	~Eye();

	void draw(Mat);
	bool isEmpty();

	operator string();

	Rect _eye_rect;
	Rect _pupil_rect;
	Point _pupil, _eye_center, _corner;
	int _eye_radius;
	bool _side;

private:
	#define RIGHT_EYE_MARK CV_RGB(255,0,0)
	#define LEFT_EYE_MARK CV_RGB(0,255,255)
	#define EYE_CORNER_MARK CV_RGB(255,255,0)
};