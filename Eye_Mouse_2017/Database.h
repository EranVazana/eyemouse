#pragma once

#include <iostream>
#include <string>
#include <vector>

#include "sqlite3.h"

using namespace std;

class Database {
public:
	typedef pair<string, int> Setting;

	#define DEFAULT_BRIGHTNESS 40
	#define DEFAULT_THRESHOLD 60
	#define DEFAULT_CONTRAST 7
	#define DEFAULT_FACE_POSITION_SENSETIVITY 7
	#define DEFAULT_EYES_POSITION_SENSETIVITY 5
	#define DEFAULT_BLUR_KSIZE_X 3
	#define DEFAULT_BLUR_KSIZE_Y 11
	#define DEFAULT_EYEBROW_REMOVE 5

	Database();
	~Database();

	int getSetting(string);
	void changeSetting(string ,int);

private:
	#define DATABASE_NAME "EyeMouseDB.db"
	#define ERROR_MESSAGE_INTRO "--(!)An Error occurred in Database: "

	sqlite3* db;

	bool checkExist();
	void openDatabase();

	void executeCommand(string, int(*)(void*, int, char**, char**));
	static int callbackSetting(void* ,int ,char** ,char**);

	char* getZErrMsg();
	char* getCustomMsg(string);
};