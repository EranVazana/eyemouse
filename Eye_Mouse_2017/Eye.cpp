#include "Eye.h"

//-Constructor.
Eye::Eye(Rect eye_rect, Rect pupil_rect, Point pupil,Point eye_center, Point corner, int eye_radius, bool side) {
	_eye_rect = eye_rect;
	_pupil_rect = pupil_rect;
	_pupil = pupil;
	_eye_center = eye_center;
	_corner = corner;
	_eye_radius = eye_radius;
	_side = side;
}

//-Empty eye Constructor.
Eye::Eye() {
	_eye_rect = Rect();
	_pupil = Point(0, 0);
	_eye_center = Point(0, 0);
	_corner = Point(0, 0);
	_eye_radius = -1;
	_side = false;
}

//-Copy Constructor.
Eye::Eye(const Eye &other){
	_eye_rect = other._eye_rect;
	_pupil = other._pupil;
	_eye_center = other._eye_center;
	_corner = other._corner;
	_side = other._side;
}

//-Destructor.
Eye::~Eye() {

}

// Input: frame from camera.
//-Draws eye position on the image.
void Eye::draw(Mat src){
	Scalar color = _side ? RIGHT_EYE_MARK : LEFT_EYE_MARK;
	circle(src, _pupil, 1, color, 2);
}

// Output: return true is eye is empty(Not found while searching).
bool Eye::isEmpty(){
	return _eye_radius == -1;
}

//-To string function.
Eye::operator string(){
	string s = "Eye Size: " + to_string(_eye_rect.width) + "," + to_string(_eye_rect.height) + "\n";
	s += "Eye Radius: " + to_string(_eye_radius) + "\n";
	s += "Pupil Size: " + to_string(_pupil_rect.width) + "," + to_string(_pupil_rect.height) + "\n";
	return s;
}