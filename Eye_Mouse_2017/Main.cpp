﻿#include "GazeTracker.h"
#include "Console.h"

//-Main application function.
int main(unsigned int argc, const char** argv) {
	//-Set the program for highest priority.
	HANDLE hProcess = GetCurrentProcess();
	SetPriorityClass(hProcess, REALTIME_PRIORITY_CLASS);

	Console::HideConsole();

	try {
		int return_code;
		GazeTracker eyeTracker;

		return_code = eyeTracker.calibrateDetectionSettings();
		if (return_code == APPLICATION_EXIT)
			return 0;

		return_code = eyeTracker.calibrateRatio();
		if (return_code == APPLICATION_EXIT)
			return 0;

		eyeTracker.mouseControl();
	}
	catch (exception exception_from_GazeTracker){
		Console::ShowConsole();
		Console::HideCursor();

		MessageBeep(MB_ICONERROR);//-Error sound.
		Console::print(_strdup(exception_from_GazeTracker.what()));

		this_thread::sleep_for(chrono::milliseconds(1500));
		Console::WaitForKeyPress();

		return -1;
	}

	return 0;
}