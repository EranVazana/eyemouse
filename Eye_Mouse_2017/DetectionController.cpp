#include "DetectionController.h"

//-Constructor.
DetectionController::DetectionController(Database &db) : _db(db){
	//-Init settings.
	_brightness = _db.getSetting("Brightness");
	_threshold = _db.getSetting("Threshold");
	_contrast = _db.getSetting("Contrast");
	_blur_ksize_X = _db.getSetting("Blur_Ksize_X");
	_blur_ksize_Y = _db.getSetting("Blur_Ksize_Y");
	_eyebrow_remove = _db.getSetting("Eyebrow_Remove");

	_face_position_sensetivity = _db.getSetting("Face_Position_Sensetivity");
	_eyes_position_sensetivity = _db.getSetting("Eyes_Position_Sensetivity");
}

//-Destructor.
DetectionController::~DetectionController(){
	close();
}

//-Open the controller window.
void DetectionController::open() {
	//-Init window
	namedWindow(CONTROLLER_WINDOW_NAME);
	resizeWindow(CONTROLLER_WINDOW_NAME, 350, 350);
	moveWindow(CONTROLLER_WINDOW_NAME, 985, 100);

	//-Create a trackbar for every setting.
	createTrackbar("Brightness", CONTROLLER_WINDOW_NAME, &_brightness, BRIGHTNESS_MAX);
	createTrackbar("Threshold", CONTROLLER_WINDOW_NAME, &_threshold, THRESHOLD_MAX);
	createTrackbar("Contrast", CONTROLLER_WINDOW_NAME, &_contrast, CONTRAST_MAX);
	createTrackbar("Blur K-X", CONTROLLER_WINDOW_NAME, &_blur_ksize_X, BLUR_MAX);
	createTrackbar("Blur K-Y", CONTROLLER_WINDOW_NAME, &_blur_ksize_Y, BLUR_MAX);
	createTrackbar("EB Removal", CONTROLLER_WINDOW_NAME, &_eyebrow_remove, EYEBROW_REMOVE_MAX);

	createTrackbar("Face Sens", CONTROLLER_WINDOW_NAME, &_face_position_sensetivity, SENSETIVITY_MAX);
	createTrackbar("Eyes Sens", CONTROLLER_WINDOW_NAME, &_eyes_position_sensetivity, SENSETIVITY_MAX);
}

void DetectionController::close() {
	//-Save the user setiings
	saveSettings();

	//-Close the controller window.
	destroyWindow(CONTROLLER_WINDOW_NAME);
}

//-Reset every setting to its defualt value.
void DetectionController::reset(){
	_brightness = DEFAULT_BRIGHTNESS;
	setTrackbarPos("Brightness", CONTROLLER_WINDOW_NAME, DEFAULT_BRIGHTNESS);

	_threshold = DEFAULT_THRESHOLD;
	setTrackbarPos("Threshold", CONTROLLER_WINDOW_NAME, DEFAULT_THRESHOLD);

	_contrast = DEFAULT_CONTRAST;
	setTrackbarPos("Contrast", CONTROLLER_WINDOW_NAME, DEFAULT_CONTRAST);

	_blur_ksize_X = DEFAULT_BLUR_KSIZE_X;
	setTrackbarPos("Blur_Ksize_X", CONTROLLER_WINDOW_NAME, DEFAULT_BLUR_KSIZE_X);

	_blur_ksize_Y = DEFAULT_BLUR_KSIZE_Y;
	setTrackbarPos("Blur_Ksize_Y", CONTROLLER_WINDOW_NAME, DEFAULT_BLUR_KSIZE_Y);

	_eyebrow_remove = DEFAULT_EYEBROW_REMOVE;
	setTrackbarPos("EB Remove", CONTROLLER_WINDOW_NAME, DEFAULT_BLUR_KSIZE_Y);

	_face_position_sensetivity = DEFAULT_FACE_POSITION_SENSETIVITY;
	setTrackbarPos("Face Sens", CONTROLLER_WINDOW_NAME, DEFAULT_FACE_POSITION_SENSETIVITY);

	_eyes_position_sensetivity = DEFAULT_EYES_POSITION_SENSETIVITY;
	setTrackbarPos("Eyes Sens", CONTROLLER_WINDOW_NAME, DEFAULT_EYES_POSITION_SENSETIVITY);
}

void DetectionController::saveSettings(){
	//-Update every setting in the database.
	_db.changeSetting("Brightness", _brightness);
	_db.changeSetting("Threshold", _threshold);
	_db.changeSetting("Contrast", _contrast);
	_db.changeSetting("Blur_Ksize_X", _blur_ksize_X);
	_db.changeSetting("Blur_Ksize_Y", _blur_ksize_Y);
	_db.changeSetting("Face_Position_Sensetivity", _face_position_sensetivity);
	_db.changeSetting("Eyes_Position_Sensetivity", _eyes_position_sensetivity);
	_db.changeSetting("Eyebrow_Remove", _eyebrow_remove);
}