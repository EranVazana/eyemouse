#pragma once

#include <iostream>

#include "opencv2/highgui.hpp"

#include "Database.h"

using namespace cv;

class DetectionController{
public:
	#define BRIGHTNESS_MAX 100
	#define THRESHOLD_MAX 255
	#define CONTRAST_MAX 10
	#define BLUR_MAX 31
	#define EYEBROW_REMOVE_MAX 8
	#define SENSETIVITY_MAX 20

	DetectionController(Database &);
	~DetectionController();

	void open();
	void close();
	void reset();
	void saveSettings();

	int _brightness;
	int _contrast;
	int _threshold;
	int _blur_ksize_X, _blur_ksize_Y;
	int _eyebrow_remove;

	int _face_position_sensetivity;
	int _eyes_position_sensetivity;

private:
	#define CONTROLLER_WINDOW_NAME "Detection Controller"
	Database _db;
};