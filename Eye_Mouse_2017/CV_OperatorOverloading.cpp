#include "CV_OperatorOverloading.h"

Point operator/(Point& p, int n) {
	return Point((int)(p.x / n), (int)(p.y / n));
}

Point& operator/=(Point& p, int n)
{
	return p = (p / n);
}

Point operator/(Point& p, XY_Amount nM) {
	return Point((int)(p.x / nM.first), (int)(p.y / nM.second));
}

Point& operator/=(Point& p, XY_Amount nM) {
	return p = p / nM;
}

Point operator+(Point& p, int n) {
	return Point(p.x + n, p.y + n);
}

Point& operator+=(Point& p, int n) {
	return p = p + n;
}

Point operator+(Point& p, XY_Amount nM) {
	return Point(p.x + nM.first, p.y + nM.second);
}

Point& operator+=(Point& p, XY_Amount nM) {
	return p = p + nM;
}

Point operator+(Point& p, Point other){
	return Point(p.x + other.x, p.y + other.y);
}
Point& operator+=(Point& p, Point other) {
	return p = p + other;
}

Point operator-(Point& p, int n) {
	return Point(p.x - n, p.y - n);
}

Point& operator-=(Point& p, int n) {
	return p = p - n;
}

Point operator-(Point& p, XY_Amount nM) {
	return Point(p.x - nM.first, p.y - nM.second);
}

Point& operator-=(Point& p, XY_Amount nM) {
	return p = p - nM;
}

Point operator-(Point& p, Point other) {
	return Point(p.x + other.x, p.y + other.y);
}

Point& operator-=(Point& p, Point other) {
	return p = p - other;
}