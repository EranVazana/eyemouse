#include "UserIdentification.h"

//-Only face found Constructor.
UserIdentification::UserIdentification(Rect face) {
	_face = face;
	_rightEye = Eye();
	_leftEye = Eye();
}

//-Both face and eyes found Constructor.
UserIdentification::UserIdentification(Rect face, Eye rightEye, Eye leftEye) {
	_face = face;
	_rightEye = rightEye;
	_leftEye = leftEye;
}

//-No face and no eyes found Constructor.
UserIdentification::UserIdentification() {
	_face = Rect();
	_rightEye = Eye();
	_leftEye = Eye();
}

//-Copy constuctor.
UserIdentification::UserIdentification(const UserIdentification & other) {
	_face = other._face;
	_rightEye = other._rightEye;
	_leftEye = other._leftEye;
}

//-Destructor.
UserIdentification::~UserIdentification(){

}

// Output: Return true if a face was found.
//-Check if the object contain values of the face.
bool UserIdentification::foundFace(){
	return _face.width != 0;
}

// Output: Return true if eyes were found.
//-Check if the object contain values of the eyes.
bool UserIdentification::foundEyes(){
	return foundFace() && (!_rightEye.isEmpty() || !_leftEye.isEmpty());
}

int UserIdentification::foundOneEye() {
	bool res = !_rightEye.isEmpty() && _leftEye.isEmpty();
	return res;
}

int UserIdentification::userWinked(UserIdentification& pirior, bool eye_side) {
	if (foundOneEye())
		return 1;
	return 0;
}

// Input: Pirior user identification values.
// Output: Return true if iser blinked, false if didn't.
//-Checks if user blinked based on the values of the current frame and the pirior one. 
bool UserIdentification::userBlinked(UserIdentification& pirior){
	//-If current frame had eyes, and the pirior didn't, return 'blink' approval.
	return ((foundFace() && foundEyes()) && (pirior.foundFace() && !pirior.foundEyes()));
}

// Output: Return the face center cordinations.
Point UserIdentification::getFaceCenter() {
	return Point(_face.x + (_face.width / 2), _face.y + (_face.height / 2));
}