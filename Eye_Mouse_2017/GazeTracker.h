#pragma once

//-Basic C++ libaries.
#include <iostream>
#include <string>
#include <algorithm>

//-Containers.
#include <vector>
#include <array>

//-For hiding and showing console. should be used in main.
#include <windows.h> 

//-Opencv libary header files.
#include <opencv2\opencv.hpp>
#include "opencv2/objdetect.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/imgproc.hpp"

//-Detection setting GUI. 
#include "DetectionController.h"

//-Identification class.
#include "UserIdentification.h"

//-OpenCV operator overloading.
#include "CV_OperatorOverloading.h"

using namespace std;
using namespace cv;

class GazeTracker {
public:
	#define APPLICATION_EXIT -1
	#define STAY_IN_APPLICATION 0

	GazeTracker();
	~GazeTracker();

	int calibrateDetectionSettings();
	int calibrateRatio();
	void mouseControl();

private:
	#define DEFAULT_DEVICE -1

	#define FACE_CASCADE_NAME "Cascades/haarcascade_frontalface_alt.xml"
	#define EYES_CASCADE_NAME "Cascades/haarcascade_eye_tree_eyeglasses.xml"

	#define NUMBER_OF_CALIBRATION_POINTS 5
	typedef array<Point, NUMBER_OF_CALIBRATION_POINTS> CalibrationPoints;

	#define AREAS_PER_ROW 5
	typedef array<array<Rect, AREAS_PER_ROW>, AREAS_PER_ROW> GazeAreas;

	typedef Size Resolution;

	void initCaptureDevice();
	void initGazeAreas();

	HWND initWindow(char* windowName);
	bool keepRunning(HWND);
	void closeWindow(HWND);

	void proceed(int);

	Mat retriveFrame();
	void addBorders(Mat&);
	void printText(Mat, string, const Point, double = 0.5, Scalar = CV_RGB(255, 0, 0));
	void displayNoDetectionWarning(Mat, const char*);
	unsigned int calculateFrameRate(double);
	char getKey();

	UserIdentification detectUserIdentification(Mat, bool, bool);
	Rect updatedRectPosition(Rect, Rect, int);
	bool getSideOfEye(Rect, Rect);
	void blurImage(Mat&);
	void removeTopRows(Mat, int);

	Point getEyePovAvgPoint();
	void calibrate(UserIdentification, unsigned int&);

	CalibrationPoints getFixedCalibrationPoints(Point, Mat);
	unsigned int getAxisValue(CalibrationPoints, UserIdentification, unsigned int, bool);
	Rect getGazeArea(Point);

	void onUserBlink(); //Temp func

	//=======================================================================

	int _return_status_code;
	
	Database _db;

	VideoCapture _capture_device;
	Resolution _capture_device_resolusion;

	Resolution _screen_resolusion;
	Point _screen_center;
	XY_Amount _borders;

	DetectionController _detection_controller;
	enum keyboard_options { CONTINUE = 13 /*Enter Key*/, QUIT = 27 /*ESC Key*/, RESET_CONTROLLER = 'r', MIRROR_FRAME = 'f' };
	
	CalibrationPoints _base_screen_calibration_points;
	CalibrationPoints _user_calibration_points;
	Point _face_center_avg;
	enum user_calibration_point_indexs { UP = 0, LEFT, MIDDLE, RIGHT, BOTTOM };

	CascadeClassifier _face_cascade;
	CascadeClassifier _eyes_cascade;
	
	GazeAreas _screen_gaze_areas;
};