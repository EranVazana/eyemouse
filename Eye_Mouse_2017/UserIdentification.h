#include "Eye.h"

class UserIdentification {
public:
	UserIdentification(Rect);
	UserIdentification(Rect, Eye, Eye);
	UserIdentification();
	UserIdentification(const UserIdentification&);
	~UserIdentification();

	bool foundFace();
	bool foundEyes();
	int foundOneEye();

	bool userBlinked(UserIdentification&);
	int userWinked(UserIdentification&, bool);

	Point getFaceCenter();

	Rect _face;
	Eye _rightEye, _leftEye;

private:

};