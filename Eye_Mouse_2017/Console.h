#pragma once

#include <iostream>
#include <string>
#include <conio.h> //-For _getch.

#include <Windows.h>

//-Thread libaries.
#include <thread>
#include <chrono>

using namespace std;

namespace Console{
	void HideConsole();
	void ShowConsole();
	void HideCursor();

	void print(char*);
	char WaitForKeyPress();
};