#include "GazeTracker.h"

//=======================================================================

#define SETTINGS_CALIBRATION_MAIN_WINDOW_NAME "EyeMouse Project - In Progress" //-For marking eyes values location.
#define RATIO_CALIBRATION_MAIN_WINDOW_NAME "Calibration" //-For setting eye to mouse ratio.
#define MOUSE_TESTING_WINDOW_NAME "Mouse Testing" //-For testing mouse movement.

#define RIGHT_EYE_PREVIEW_WINDOW_NAME "Right Eye Settings Preview" //-For viewing right pupil detction setting.
#define RIGHT_EYE_THRESHED_PREVIEW_WINDOW_NAME "Right Pupil Detection Preview" //-For viewing right pupil detection. 

#define LEFT_EYE_PREVIEW_WINDOW_NAME "Left Eye Settings Preview" //-For viewing left pupil detction setting.
#define LEFT_EYE_THRESHED_PREVIEW_WINDOW_NAME "Left Pupil Detection Preview" //-For viewing left pupil detection. 

#define FPS_TEXT_LOCATION Point(5,15)
#define RESET_CONTROLLER_LOCATION Point(350, 15)
#define CONTINUE_TEXT_LOCATION Point(5,475)

//=======================================================================
#define NO_EYE_WARNING_TEXT "No Eyes Detected!"
#define NO_EYE_WARNING_LOCATION Point(_borders.first + (int)(_capture_device_resolusion.width * 0.125), (int)(_borders.second * 0.75))

#define AVG_ITERATION_NUMBER 10

#define MOUSE_MARK_SIZE 10

//=======================================================================

bool keep_alive = true; //-To continue calibrating or exit.
bool mirror_frame = true; //-For fliping device input for more convenience calibration. (Mirror image)

//=======================================================================

//-Constructor.
GazeTracker::GazeTracker() : _detection_controller(_db) {
	//-Load the face cascade.
	if (!_face_cascade.load(FACE_CASCADE_NAME)) 
		throw exception("--(!)An Error occurred while loading face cascade.");

	//-Load the eyes cascade.
	if (!_eyes_cascade.load(EYES_CASCADE_NAME)) 
		throw exception("--(!)An Error occurred while loading eyes cascade.");

	//-Open camera.
	initCaptureDevice();
		
	//-Get screen resolution values.
	unsigned int screen_width = GetSystemMetrics(SM_CXSCREEN);
	unsigned int screen_height = GetSystemMetrics(SM_CYSCREEN);

	_screen_resolusion = Resolution(screen_width, screen_height);
	_screen_center = Point(screen_width / 2, screen_height / 2);

	//-Get device resolution values.
	_capture_device_resolusion = Resolution((int)_capture_device.get(CAP_PROP_FRAME_WIDTH), (int)_capture_device.get(CAP_PROP_FRAME_HEIGHT));

	//-Get borders values.
	_borders = XY_Amount((int)((screen_width - _capture_device_resolusion.width) / 2), (int)((screen_height - _capture_device_resolusion.height) / 2));

	//-Initalize calibration points cordinations.
	_base_screen_calibration_points = { Point((int)(screen_width / 2), 0),
										Point(0, (int)(screen_height / 2)), Point((int)(screen_width / 2), screen_height / 2), Point((int)(screen_width), screen_height / 2),
										Point((int)(screen_width / 2), screen_height) };

	//-Init gaze areas for the mouse.
	initGazeAreas();

	_return_status_code = APPLICATION_EXIT;
}

//-Destructor.
GazeTracker::~GazeTracker() {
	destroyAllWindows();
	_capture_device.release();
}

//-Try opening X amount of devices until one is open.
//-If no device was found, throw an exception to the main function.
void GazeTracker::initCaptureDevice() {
	const unsigned int maxDevices = 10;
	for (int device = 0; device < maxDevices; device++) {
		_capture_device = VideoCapture(device);
		if (_capture_device.isOpened())
			return;
		_capture_device.release();
	}

	//-Throw exception if no device is open.
	throw exception("--(!)An Error occurred while opening video capture.");
}

//-Initialize the screen gaze areas.
void GazeTracker::initGazeAreas() {
	//-Calculate the width and height for each area based on the screen resolution values.
	//-Upper the results in case of a non-integer number (For resolutions such as 1360x768).
	unsigned int area_width = (int)ceil((float)_screen_resolusion.width / AREAS_PER_ROW);
	unsigned int area_height = (int)ceil((float)_screen_resolusion.height / AREAS_PER_ROW);

	//-Init each rect in the multidimensional array.
	for (unsigned int i = 0; i < AREAS_PER_ROW; i++)
		for (unsigned int j = 0; j < AREAS_PER_ROW; j++)
			_screen_gaze_areas[i][j] = Rect(j * area_width, i * area_height, area_width, area_height);
}

// Input: Name of the window.
// Output: Pointer of the window.
//-Initialize the window to fullscreen with no black borders.
HWND GazeTracker::initWindow(char* window_name) {
	//-Set window to full screen.
	namedWindow(window_name, CV_WINDOW_NORMAL);
	setWindowProperty(window_name, CV_WND_PROP_FULLSCREEN, CV_WINDOW_FULLSCREEN);

	//-Delete window borders.
	HWND window = FindWindow(NULL, window_name);
	SetClassLongPtr(window, GCLP_HBRBACKGROUND, (LONG)CreateSolidBrush(RGB(0, 0, 0)));

	//-Return the pointer of the window.
	return window;
}

// Input: Window of the current state of the program.
// Output: If window is alive or if user wished to close the program.
bool GazeTracker::keepRunning(HWND window){
	return IsWindowVisible(window) && keep_alive;
}

// Input: Window of the current state of the program.
//-Close all windows and release the handle.
void GazeTracker::closeWindow(HWND window){
	SendMessage(window, WM_CLOSE, NULL, NULL);
	keep_alive = true;
}

// Input: User's option(exit program or continue to the next step).
//-Init the return status code and the frame capturing loop condition.
void GazeTracker::proceed(int option) {
	_return_status_code = option;
	keep_alive = false;
}

//-Function used to let the user adjust the settings for face and eye detection. 
int GazeTracker::calibrateDetectionSettings() {
	//-Set windows positions.
	namedWindow(SETTINGS_CALIBRATION_MAIN_WINDOW_NAME, CV_WINDOW_NORMAL);
	resizeWindow(SETTINGS_CALIBRATION_MAIN_WINDOW_NAME, 640, 480);
	moveWindow(SETTINGS_CALIBRATION_MAIN_WINDOW_NAME, 330, 100);

	namedWindow(RIGHT_EYE_PREVIEW_WINDOW_NAME, CV_WINDOW_AUTOSIZE);
	moveWindow(RIGHT_EYE_PREVIEW_WINDOW_NAME, 40, 100);

	namedWindow(RIGHT_EYE_THRESHED_PREVIEW_WINDOW_NAME, CV_WINDOW_AUTOSIZE);
	moveWindow(RIGHT_EYE_THRESHED_PREVIEW_WINDOW_NAME, 40, 200);

	namedWindow(LEFT_EYE_PREVIEW_WINDOW_NAME, CV_WINDOW_AUTOSIZE);
	moveWindow(LEFT_EYE_PREVIEW_WINDOW_NAME, 180, 100);

	namedWindow(LEFT_EYE_THRESHED_PREVIEW_WINDOW_NAME, CV_WINDOW_AUTOSIZE);
	moveWindow(LEFT_EYE_THRESHED_PREVIEW_WINDOW_NAME, 180, 200);

	//-Open the controller window.
	_detection_controller.open();

	//-Get the main window handle.
	HWND settings_preview_window = (HWND)cvGetWindowHandle(SETTINGS_CALIBRATION_MAIN_WINDOW_NAME);

	UserIdentification pirior_user_identification;
	while (keepRunning(settings_preview_window)){
		//-Start counting frames.
		double tick = (double)getTickCount();

		Mat frame = retriveFrame();

		//-Get user face and eye locations and values.
		UserIdentification current_user_identification = detectUserIdentification(frame, true, true);

		//-Perform 'Blink' test
		if (current_user_identification.userWinked(pirior_user_identification,false)) {
			printText(frame, "Blink", Point(200, 200));
			onUserBlink();
		}

		//-Save the current user identification.
		pirior_user_identification = current_user_identification;

		//-Calculate how much frames were processed.
		tick = calculateFrameRate(tick);
		string fps_text = "FPS: " + to_string((int)tick);

		printText(frame, fps_text, FPS_TEXT_LOCATION);
		printText(frame, "Press 'R' to reset the controller.", RESET_CONTROLLER_LOCATION);
		printText(frame, "Press 'Enter' to enter mouse calibration.", CONTINUE_TEXT_LOCATION);

		imshow(SETTINGS_CALIBRATION_MAIN_WINDOW_NAME, frame);

		switch (getKey()){
			case (RESET_CONTROLLER):
				_detection_controller.reset();
				break;
			case (MIRROR_FRAME):
				mirror_frame = !mirror_frame;
				break;
			case (QUIT):
				proceed(APPLICATION_EXIT);
				break;
			case (CONTINUE):
				proceed(STAY_IN_APPLICATION);
				break;
		}
			
	}

	//-Close the controller window.
	_detection_controller.close();

	//-Close the windows.
	closeWindow(settings_preview_window);
	destroyAllWindows();

	return _return_status_code;
}

//-Function used to let the user calibrate the eye movment to mouse position ratio.
int GazeTracker::calibrateRatio(){
	//-Initialize the window.
	HWND ratio_calibration_window = initWindow(RATIO_CALIBRATION_MAIN_WINDOW_NAME);

	//-Set the screen calibration points colors.
	Scalar primary_color = CV_RGB(255, 0, 0);
	Scalar secondery_color = CV_RGB(0, 0, 255);

	unsigned int points_array_index = 0;

	while (keepRunning(ratio_calibration_window)){
		Mat frame = retriveFrame();

		UserIdentification current_user_identification = detectUserIdentification(frame, true, false);
		if (!current_user_identification.foundEyes()) {
			displayNoDetectionWarning(frame, RATIO_CALIBRATION_MAIN_WINDOW_NAME);
			continue;
		}

		//-Add borders to the frames.
		addBorders(frame);

		for (int i = 0; i < AREAS_PER_ROW; i++)
			for (int j = 0; j < AREAS_PER_ROW; j++)
				rectangle(frame, _screen_gaze_areas[i][j], CV_RGB(0, 128, 255));

		//-Draw the calibration points.
		circle(frame, _base_screen_calibration_points[points_array_index], MOUSE_MARK_SIZE, primary_color, FILLED);
		circle(frame, _base_screen_calibration_points[points_array_index], MOUSE_MARK_SIZE / 2, secondery_color, FILLED);

		imshow(RATIO_CALIBRATION_MAIN_WINDOW_NAME, frame);

		switch (getKey()){
			case (MIRROR_FRAME) :
				mirror_frame = !mirror_frame;
				break;
			case (QUIT):
				proceed(APPLICATION_EXIT);
				break;
			case (CONTINUE) :
				calibrate(current_user_identification, points_array_index);
				break;
		}

		swap(primary_color, secondery_color);
	}

	closeWindow(ratio_calibration_window);
	return _return_status_code;
}

void GazeTracker::mouseControl(){
	//-Initialize the window.
	HWND ratio_preview_window = initWindow(MOUSE_TESTING_WINDOW_NAME);

	UserIdentification pirior_user_identification;
	Point current_mouse_location, pirior_mouse_loactaion;

	bool is_zooming = false;

	Rect selected_area;

	while (keepRunning(ratio_preview_window)){
		Mat frame = retriveFrame();

		UserIdentification current_user_identification = detectUserIdentification(frame, true, false);
		if (!current_user_identification.foundEyes()) {
			displayNoDetectionWarning(frame, MOUSE_TESTING_WINDOW_NAME);
		}
		Eye left_eye = current_user_identification._leftEye;
		addBorders(frame);

		if (is_zooming)
			frame = frame(selected_area);

		CalibrationPoints fixed_calibration_points = getFixedCalibrationPoints(current_user_identification.getFaceCenter(),frame);

		circle(frame, _face_center_avg + _borders, 5, CV_RGB(0, 255, 0), FILLED);

		/*
		Point base = base_identification.pupilsDistanceCenter();
		base.x = _border_amount_LR + base.x;
		base.y = _border_amount_TB + base.y;

		circle(frame, base, 5, CV_RGB(255, 0, 255), FILLED);

		Point pointer = current_user_identification.pupilsDistanceCenter();
		pointer.x = _border_amount_LR + pointer.x;
		pointer.y = _border_amount_TB + pointer.y;

		circle(frame, pointer, 5, CV_RGB(0, 255, 0),FILLED);

		int subx = pointer.x - base.x;
		int suby = pointer.y - base.y;

		Point area(base.x + (a * subx), base.y + (a * suby));
		circle(frame, area, 5, CV_RGB(0, 255, 0), FILLED);

		//rectangle(frame,)

		imshow(MOUSE_TESTING_WINDOW_NAME, frame);
		char keyboard_key = waitKey(1);
		if (keyboard_key == 27)
			keep_alive = false;
		else if (keyboard_key == 'z')
			a++;
		else if (keyboard_key == 'x')
			a--;
			*/
		
		/*
		Point a = current_user_identification._leftEye._corner;
		Point b = current_user_identification._leftEye_pupil;
		Point c = tempEye._corner;
		Point d = tempEye._pupil;

		double e = b.x - a.x;
		double f = d.x - c.x;
		double t = (a.y / b.y);
		double newx = (e / f) * 683;
		double newy = (t) * 384;
		*/
		/*
		double r = left_eye._eye_radius;
		double d = current_user_identification._face.width * 5;
		int subx = left_eye._pupil.x - left_eye._eye_center.x;
		int suby = left_eye._pupil.y - left_eye._eye_center.y;

		double t = (d + r) / r;

		int newx = _screen_center_x + (int)(r * 3.5 * subx);
		int newy = _screen_center_x + (int)(r * 3.5 * suby);
		*/
		/*
		//int basex = _border_amount_LR + ((right_eye._eye_center.x + left_eye._eye_center.x) / 2);
		//int basey = _border_amount_TB + ((right_eye._eye_center.y + left_eye._eye_center.y) / 2);

		int basex = _border_amount_LR + face.x + face.width / 2;
		int basey = _border_amount_TB + face.y + face.height / 2 - 27;

		int ecx = _border_amount_LR + face.x + face.width * 0.33 - 1;
		int ecy = _border_amount_TB + face.y + face.height * 0.41 - 1;
		circle(frame, Point(ecx, ecy), 5, CV_RGB(255, 0, 0), FILLED);

		int subx = _border_amount_LR + right_eye._pupil.x - ecx;
		int suby = _border_amount_TB + right_eye._pupil.y - ecy;

		double r = right_eye._eye_radius;

		int newx = basex + (3.5 * subx); 
		int newy = basey + (suby); 

		circle(frame, Point(basex, basey), 5, CV_RGB(255, 0, 0), FILLED);
		*/
		/*int basex = _border_amount_LR + face.x + face.width / 2;
		int basey = _border_amount_TB + face.y + face.height / 2 - 27;

		circle(frame, Point(basex, basey), 5, CV_RGB(255, 0, 0), FILLED);

		int newx = _border_amount_LR + current_user_identification.pupilsDistanceCenter().x;
		int newy = _border_amount_TB + left_eye._pupil.y;

		circle(frame, Point(newx, newy), 5, CV_RGB(255, 255, 0), FILLED);

		int subx = (newx - basex);
		int suby = (newy - basey);

		if (abs(subx) <  && abs(suby)  < 3){
			subx = 0;
			suby = 0;
		}

		int x =  basex + (subx * 60);
		int y =  basey + (suby * 30);

		circle(frame, Point(x, y), 5, CV_RGB(0, 255, 0), FILLED);
		*/
		/*int basex = _border_amount_LR + face.x + face.width / 2;
		int basey = _border_amount_TB + face.y + face.height / 2 - 27;

		basex = _border_amount_LR + face.x + face.width * 0.511;
		basey = _border_amount_TB + face.y + face.height /2  * 0.982;

		circle(frame, Point(basex, basey), 5, CV_RGB(255, 0, 0), FILLED);

		int newx = _border_amount_LR + current_user_identification.pupilsDistanceCenter().x;
		int newy = _border_amount_TB + left_eye._pupil.y;

		circle(frame, Point(newx, newy), 5, CV_RGB(255, 255, 0), FILLED);

		int subx = (newx - basex);
		int suby = (newy - basey);

		if (abs(subx) < 4 && abs(suby) < 4){
			subx = 0;
			suby = 0;
		}

		int x =  basex + (subx * 60);
		int y =  basey + (suby * 30);

		current_frame = Point(x, y);
		true_frame.x = (last_frame.x +  current_frame.x) / 2;
		true_frame.y = (last_frame.y +  current_frame.y) / 2;

		circle(frame, true_frame, 5, CV_RGB(0, 255, 0), FILLED);
		
		string text = to_string(newx) + "," + to_string(newy) + " " + to_string(face.width) + " " + to_string(face.height);
		putText(frame, text, Point(400, 400), FONT_HERSHEY_PLAIN, 0.8, CV_RGB(0, 0, 0), 1, CV_AA);
		*/
		
		/*
	    Point base = current_user_identification.baseCenter();
		base.x = _border_amount_LR + face.x + base.x + fixX;
		base.y = _border_amount_TB + face.y + base.y + fixY;

		circle(frame, base, 5, CV_RGB(255, 0, 0), FILLED);

		Point pupilCenterLocation = current_user_identification.pupilsDistanceCenter();
		pupilCenterLocation.x += _border_amount_LR;
		pupilCenterLocation.y += _border_amount_TB;

		circle(frame, pupilCenterLocation, 5, CV_RGB(0, 0, 255), FILLED);

		int subx = pupilCenterLocation.x - base.x;
		int suby = pupilCenterLocation.y - base.y;

		Point cur(base.x + (50 * subx), base.y + (50 * suby));
		true_frame.x = (last_frame.x + cur.x) / 2;
		true_frame.y = (last_frame.y + cur.y) / 2;
		
		if (show_mouse_place)
			circle(frame, true_frame, 5, CV_RGB(0, 255, 0), FILLED);

		imshow(MOUSE_TESTING_WINDOW_NAME, frame);

		char keyboard_key = waitKey(1);
		if (keyboard_key == 27)
			keep_alive = false;
		else if (keyboard_key == 'd')
			fixX++;
		else if (keyboard_key == 'a')
			fixX--;
		else if (keyboard_key == 'w')
			fixY--;
		else if (keyboard_key == 's')
			fixY++;
		else if (keyboard_key == 13)
			show_mouse_place = true;
	*/

		/*
		if (left_eye._pupil.x < _user_calibration_points[MIDDLE].x) {
			circle(frame, _base_screen_calibration_points[LEFT], 10, CV_RGB(255, 0, 0), FILLED);
			unsigned int x = getAxisValue(left_eye, LEFT, true);
			circle(frame, Point(x, _screen_center.y), 10, CV_RGB(0, 255, 0), FILLED);
		}
		else {
			circle(frame, _base_screen_calibration_points[RIGHT], 10, CV_RGB(255, 0, 0), FILLED);
			unsigned int x = getAxisValue(left_eye, RIGHT, true);
			circle(frame, Point(x, _screen_center.y), 10, CV_RGB(0, 255, 0), FILLED);
		}

		if (left_eye._pupil.y < _user_calibration_points[MIDDLE].y)
			circle(frame, _base_screen_calibration_points[0], 10, CV_RGB(0, 0, 255), FILLED);
		else
			circle(frame, _base_screen_calibration_points[4], 10, CV_RGB(0, 0, 255), FILLED);
		*/
		
		Point current_mouse_location;
		if (left_eye._pupil.x < fixed_calibration_points[MIDDLE].x)
			current_mouse_location.x = getAxisValue(fixed_calibration_points, current_user_identification, LEFT, true);
		else
			current_mouse_location.x = getAxisValue(fixed_calibration_points, current_user_identification, RIGHT, true);

		if (left_eye._pupil.y < fixed_calibration_points[MIDDLE].y)
			current_mouse_location.y = getAxisValue(fixed_calibration_points, current_user_identification, UP, false);
		else
			current_mouse_location.y = getAxisValue(fixed_calibration_points, current_user_identification, BOTTOM, false);

		current_mouse_location.x = (pirior_mouse_loactaion.x + current_mouse_location.x) / 2;
	    current_mouse_location.y = (pirior_mouse_loactaion.y + current_mouse_location.y) / 2;

		if (current_mouse_location.x > _screen_resolusion.width)
			current_mouse_location.x = _screen_resolusion.width - 1;

		if (current_mouse_location.y > _screen_resolusion.height)
			current_mouse_location.y = _screen_resolusion.height - 1;

		Rect gaze_area = getGazeArea(current_mouse_location);
		Point area_center = Point(gaze_area.x + gaze_area.width / 2, gaze_area.y + gaze_area.height / 2);

		rectangle(frame, gaze_area, CV_RGB(255,0,0));
		circle(frame, area_center, 10, CV_RGB(0, 255, 0), FILLED);

		if (current_user_identification.userBlinked(pirior_user_identification)) {
			onUserBlink();
			selected_area = gaze_area;
			is_zooming = !is_zooming;
		}

		imshow(MOUSE_TESTING_WINDOW_NAME, frame);

		if (getKey() == QUIT)
			break;

		pirior_mouse_loactaion = current_mouse_location;
		pirior_user_identification = current_user_identification;
		frame.release();
	}

	closeWindow(ratio_preview_window);
}

// Output: frame from the capture device.
//-Retriving frame from the capture device, mirror the image(unless the user told not to) for better tracking results.
//-throws exception if the capture device disconnected.
Mat GazeTracker::retriveFrame(){
	Mat new_frame;

	//-Capture frame from the capture device.
	if (!_capture_device.retrieve(new_frame))
		throw exception("--(!)An Error occurred while capturing frames: Capture device disconnected.");

	//-Mirror the image.
	if (mirror_frame)
		flip(new_frame, new_frame, 1);

	return new_frame;
}

// Input: frame from the caputre device.
//-Adds white borders to the frame on each side.
void GazeTracker::addBorders(Mat &frame) {
	copyMakeBorder(frame, frame, _borders.second, _borders.second, _borders.first, _borders.first, BORDER_CONSTANT, Scalar(255, 255, 255));
}

// Input: Frame from the capture device, text, location(Point).
// Optional Input: Color, scaling size.
//-Print text on a frame.
void GazeTracker::printText(Mat frame, string text, const Point location, double font_scale, Scalar color) {
	putText(frame, text, location, FONT_HERSHEY_TRIPLEX, font_scale, color, 1, CV_AA);
}

// Input: Frame from the capture device, window name.
//-Display a frame with a warning about the detection of the eyes.
void GazeTracker::displayNoDetectionWarning(Mat frame, const char* window_name) {
	addBorders(frame);

	printText(frame, NO_EYE_WARNING_TEXT, NO_EYE_WARNING_LOCATION, 1.5);

	imshow(window_name, frame);
	if (getKey() == QUIT)
		proceed(APPLICATION_EXIT);
}

// Input: Number of ticks passed.
// Output: Number of frames processed.
//-Calculate how much frames were processed.
unsigned int GazeTracker::calculateFrameRate(double tick){
	tick = ((double)getTickCount() - tick) / getTickFrequency();
	return (unsigned int)((1.0 / tick));
}

// Output: Value of the key pressed by the user.
//-Returns the lowercase value of the key. Delay time is set to 1 in order to get the minimal delay posibble.
char GazeTracker::getKey() {
	return tolower(waitKey(1));
}

//=======================================================================

UserIdentification pirior_identification;
// Input: Frame from the camera,init the user's eyes.
// Output: UserIdefntification values that stores the face and eyes position.
//-Detects user's eyes and face and draws their location on the frame.
UserIdentification GazeTracker::detectUserIdentification(Mat src, bool to_draw = false, bool show_process = false) {
	//-Change the src image to a gray image.
	Mat frame_gray;
	cvtColor(src, frame_gray, CV_BGR2GRAY);

	//-Find faces in the gray frame image.
	vector<Rect> faces;
	_face_cascade.detectMultiScale(frame_gray, faces, 1.1, 2, 0 | CASCADE_SCALE_IMAGE, Size(150, 150));
	if (faces.empty())
		return UserIdentification();//-Return nothing found;

	Rect user_face = faces[0];

	//-Check to see if it's probably the same user                                          
	if (abs(user_face.x - pirior_identification._face.x) < src.size().width / 6 && abs(user_face.y - pirior_identification._face.y) < src.size().height / 6)
		user_face = updatedRectPosition(user_face, pirior_identification._face, _detection_controller._face_position_sensetivity);

	//-Crop the src image to only keep the face.
	Mat faceROI = frame_gray(user_face);
	frame_gray.release();

	//-Draw the face of the user.
	if (to_draw)
		rectangle(src, user_face, CV_RGB(0, 255, 0));

	//-Detect eyes in the faceROI.
	vector<Rect> eyes;
	_eyes_cascade.detectMultiScale(faceROI, eyes, 1.1, 2, 0 | CASCADE_SCALE_IMAGE, Size(15, 15), Size(70, 70));
	if (eyes.empty())
		return UserIdentification(user_face);//-Return only face found.

	Eye right_eye, left_eye;
	for (unsigned int i = 0; i < eyes.size(); i++) {
		//-Check if current rect is in a normal eye position.
		if (user_face.y + eyes[i].y > user_face.y + user_face.width / 2)
			continue;

		bool eye_side = getSideOfEye(user_face, eyes[i]);//-Holds true if right,false if left.

		Rect current_eye = eye_side ? updatedRectPosition(eyes[i], pirior_identification._rightEye._eye_rect, _detection_controller._eyes_position_sensetivity) :
			updatedRectPosition(eyes[i], pirior_identification._leftEye._eye_rect, _detection_controller._eyes_position_sensetivity);

		//-Crop the eye image from the face image and add brightness.
		Mat eyeROI = faceROI(current_eye).clone() + Scalar(_detection_controller._brightness, _detection_controller._brightness, _detection_controller._brightness);

		//-Change contrast.
		eyeROI.convertTo(eyeROI, -1, (double)_detection_controller._contrast / 10, 0);

		//-Blur the ROI.
		blurImage(eyeROI);

		if (show_process)
			eye_side ? imshow(RIGHT_EYE_PREVIEW_WINDOW_NAME, eyeROI) : imshow(LEFT_EYE_PREVIEW_WINDOW_NAME, eyeROI);

		//-Threshold the eye image to a binary image.
		threshold(eyeROI, eyeROI, _detection_controller._threshold, THRESHOLD_MAX, CV_THRESH_BINARY_INV);

		removeTopRows(eyeROI, (int)(eyeROI.rows / (10 - _detection_controller._eyebrow_remove)));

		//-Dilate(fill) the contours.
		unsigned int dilation_size = 1;
		Mat element = getStructuringElement(0,
			Size(2 * dilation_size + 1, 2 * dilation_size + 1),
			Point(dilation_size, dilation_size));
		///-Apply the dilation operation
		dilate(eyeROI, eyeROI, element);

		if (show_process)
			eye_side ? imshow(RIGHT_EYE_THRESHED_PREVIEW_WINDOW_NAME, eyeROI) : imshow(LEFT_EYE_THRESHED_PREVIEW_WINDOW_NAME, eyeROI);

		//-Find contours in the thresholded image.
		vector<vector<Point>> contours;
		findContours(eyeROI.clone(), contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
		drawContours(eyeROI, contours, -1, CV_RGB(255, 255, 255), 3);

		double max_area = 0;
		Rect pupil_rect;
		int pupil_radius_X;
		int pupil_radius_Y;

		//-Find the biggest rect.
		for (unsigned int j = 0; j < contours.size(); j++) {
			double area = contourArea(contours[j]);

			if (area > max_area) {
				max_area = area;
				pupil_rect = boundingRect(contours[j]);
				pupil_radius_X = (int)(pupil_rect.width / 2);
				pupil_radius_Y = (int)(pupil_rect.height / 2);
			}
		}

		//-If a rect have been found, update eye location.
		if (max_area != 0) {
			Point eye_pupil(user_face.x + current_eye.x + pupil_rect.x + pupil_radius_X, user_face.y + current_eye.y + pupil_rect.y + pupil_radius_Y);
			Point eye_center(user_face.x + current_eye.x + current_eye.width / 2, user_face.y + current_eye.y + current_eye.height / 2);
			Point eye_corner = eye_side ? Point(user_face.x + current_eye.x, user_face.y + current_eye.y + current_eye.height / 2 + 2) : Point(user_face.x + current_eye.x + current_eye.width, user_face.y + current_eye.y + current_eye.height / 2 + 2);
			int eye_radius = cvRound((current_eye.width + current_eye.height) * 0.25);

			eye_side ? right_eye = Eye(current_eye, pupil_rect, eye_pupil, eye_center, eye_corner, eye_radius, eye_side) : left_eye = Eye(current_eye, pupil_rect, eye_pupil, eye_center, eye_corner, eye_radius, eye_side);
		}

	}

	//-Draw the pupil location.
	if (to_draw) {
		right_eye.draw(src);
		left_eye.draw(src);
	}

	//-Save current identification values.
	UserIdentification current_identification(user_face, right_eye, left_eye);
	if (to_draw)
		circle(src, current_identification.getFaceCenter(), 5, CV_RGB(255, 128, 0), FILLED);

	pirior_identification = current_identification;
	return current_identification;//-Return both face and eyes found.
}

// Input: Current rect, pirior rect and sensetivity.
// Output: Most precise location of the rect dependet on the user prefernece.
//-Check to see if the user moved enough to update position of the rect.                           
Rect GazeTracker::updatedRectPosition(Rect current_rect, Rect pirior_rect, int rect_sensetivity){
	return (abs(current_rect.x - pirior_rect.x) < rect_sensetivity && abs(current_rect.y - pirior_rect.y) < rect_sensetivity) ? pirior_rect : current_rect;
}

// Input: Face of the user and one of his eyes.
// Output: Return true if is right eye,false if left.
//-Function used to get the side of the eye on the face of the user.
bool GazeTracker::getSideOfEye(Rect face, Rect eye) {
	return (float)eye.x < (face.width / 2);
}

// Input: ROI of the eye.
//-Blur the eye image to get rid of unnecessary noises(eye lashes,etc..) 
void GazeTracker::blurImage(Mat& eyeROI) {
	int kernel_size_X = _detection_controller._blur_ksize_X;
	int kernel_size_Y = _detection_controller._blur_ksize_Y;

	//-Since the OpenCV blur function only accept odd integers, we add 1 to the integer to make sure its an odd number.
	if (kernel_size_X % 2 == 0 || kernel_size_X == 0)
		kernel_size_X++;

	if (kernel_size_Y % 2 == 0 || kernel_size_Y == 0)
		kernel_size_Y++;

	//-After correcting the controller values, we can now apply the blur to the ROI.
	blur(eyeROI, eyeROI, Size(kernel_size_X, kernel_size_Y));
}

// Input: frame from camera,number of rows to remove.
//-Removes top layers of the mat to hide user's eyebrows.
void GazeTracker::removeTopRows(Mat src, int number_of_rows) {
	for (int y = 0; y < number_of_rows; y++) {
		//-Get pointer of the row.
		float* current_row_ptr = src.ptr<float>(y);
		for (int x = 0; x < src.cols; x++)
			current_row_ptr[x] = 0.; //-Paint pixel value to black (using float values).
	}
}

//=======================================================================

// Output: Return the average point of the user point of view.
//-Capture frames X amount of times and return the avarage point of the user point of view.
Point GazeTracker::getEyePovAvgPoint() {
	Point avg_point;

	for (unsigned int iteration = 0; iteration < AVG_ITERATION_NUMBER; iteration++) {
		Mat frame = retriveFrame();

		UserIdentification current_user_identification = detectUserIdentification(frame, false, false);
		if (!current_user_identification.foundEyes())
			continue;

		avg_point += current_user_identification._leftEye._pupil;
	}

	return (avg_point / AVG_ITERATION_NUMBER);
}

// Input: User identification, index of the calibration points array. 
//-Save the avarge eye pov in the array, calculate the avarage face position. Once the array is full the function will update the caller function frame capture loop.
//-The function auto increase the array index.
void GazeTracker::calibrate(UserIdentification current_user_identification, unsigned int& index) {
	//-Save avarage eye POV.
	_user_calibration_points[index] = getEyePovAvgPoint();

	//-Get the current face center value.
	_face_center_avg += current_user_identification.getFaceCenter();

	index++;
	if (index == NUMBER_OF_CALIBRATION_POINTS) {
		//Avarage the face center position.
		_face_center_avg /= NUMBER_OF_CALIBRATION_POINTS;

		proceed(STAY_IN_APPLICATION);
	}
}

GazeTracker::CalibrationPoints GazeTracker::getFixedCalibrationPoints(Point user_face_center, Mat frame) {
	//-Copy the calibrated user points.
	CalibrationPoints adjusted_points = _user_calibration_points;

	int subx = _face_center_avg.x - user_face_center.x;
	int suby = _face_center_avg.y - user_face_center.y;

	for (int i = 0; i < NUMBER_OF_CALIBRATION_POINTS; i++)
		adjusted_points[i] -= 0;

	//adjusted_points[MIDDLE] -= XY_Amount(subx, suby);

	return adjusted_points;
}

unsigned int GazeTracker::getAxisValue(CalibrationPoints point_array, UserIdentification user_identification ,unsigned int side, bool axis) {
	int n = (side == UP || side == LEFT) ? -1 : 1;

	int a = axis ? point_array[MIDDLE].x - user_identification._leftEye._pupil.x : point_array[MIDDLE].y - user_identification._leftEye._pupil.y;
	int b = axis ? point_array[MIDDLE].x - point_array[side].x : point_array[MIDDLE].y - point_array[side].y;

	float c = 1 + n * ((float)a / (float)b);

	return (unsigned int) (axis ? _screen_center.x * c : _screen_center.y * c);
}

Rect GazeTracker::getGazeArea(Point mouse_location) {
	for (int i = 0; i < AREAS_PER_ROW; i++)
		for (int j = 0; j < AREAS_PER_ROW; j++)
			if (_screen_gaze_areas[i][j].contains(mouse_location))
				return _screen_gaze_areas[i][j];

	return _screen_gaze_areas[2][2];
}

//-Function used for executing blink command.
void GazeTracker::onUserBlink(){
	//-Temp command.
	Beep(1500, 100);
}