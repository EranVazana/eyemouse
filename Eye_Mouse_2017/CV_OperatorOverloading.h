#pragma once
#include <iostream>

#include <opencv2\opencv.hpp>

using namespace cv;

typedef std::pair<int, int> XY_Amount;

Point operator/(Point&, int);
Point& operator/=(Point&, int);
Point operator/(Point&, XY_Amount);
Point& operator/=(Point&, XY_Amount);

Point operator+(Point&, int);
Point& operator+=(Point&, int);
Point operator+(Point&, XY_Amount);
Point& operator+=(Point&, XY_Amount);
Point operator+(Point&, Point);
Point& operator+=(Point&, Point);

Point operator-(Point&, int);
Point& operator-=(Point&, int);
Point operator-(Point&, XY_Amount);
Point& operator-=(Point&, XY_Amount);
Point operator-(Point&, Point);
Point& operator-=(Point&, Point);