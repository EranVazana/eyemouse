###Eye Mouse###
Eye Mouse is an OpenCV based C++ program that give the user the ability to control the mouse cursor with his eyes.

The program can detect the user eyes on different lighting by letting the user control the "Correctness" of different factors(Such as contrast, blur, etc..)

### Created by: ###
Eran Vazana.

Alon Wasserman.

### Requirments ###
* Decent lighting around the user area.
* A Webcam capable of taking pictures at 480p or more.(On higher resolution Webcams, we recomend to set the resolution for 480p, Any resolution beyond 480p will cause the program to run slower)

### Running the program ###
To run the program just simply run the EXE file and make sure the 'Cascades' folder is in the same folder.